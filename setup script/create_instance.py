# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import boto
from boto.ec2.regioninfo import RegionInfo
import time

EC2_ACCESS_KEY_ID = "4f2f5920cd494f559aad17720e83058c"
EC2_SECRET_KEY_ID = "bdc64e272f854bf0900b45945d413f7d"
# NeCTAR Ubuntu 16.04 LTS (Xenial) amd64
image_id = "ami-00003b2e"
max_check = 20

# connect to the cloud
region = RegionInfo(name="melbourne", endpoint="nova.rc.nectar.org.au")
connection = boto.connect_ec2(aws_access_key_id=EC2_ACCESS_KEY_ID,
                                  aws_secret_access_key=EC2_SECRET_KEY_ID,
                                  is_secure=True, region=region, port=8773,
                                  path='/services/Cloud', validate_certs=False)

# images = connection.get_all_images()
# for image in images:
#     if image.name == "NeCTAR Ubuntu 16.04 LTS (Xenial) amd64":
#         print image.id
#         print image.name


# check whether the instance is running
def check_status(instance_num):
    loop = 0
    while loop < max_check:
        all_running = True
        reservations = connection.get_all_reservations()
        for reservation in reservations:
            for instance in reservation.instances:
                if instance.state != 'running':
                    all_running = False
        if all_running:
            return True
        else:
            time.sleep(15)
        loop += 1

    return False

# create instance
instance_num = 1
for i in range(instance_num):
    connection.run_instances(image_id, key_name='cloud', instance_type="m2.medium", security_groups=['SSH', 'http'])


if check_status(instance_num):
    my_file = open("hosts", "w")
    my_file.write("[cloud]\n")
    reservations = connection.get_all_reservations()
    for reservation in reservations:
        for instance in reservation.instances:
            print instance.id, instance.private_ip_address
            my_file.write(instance.private_ip_address+"\n")
    my_file.write("\n[cloud:vars]\nansible_ssh_user=ubuntu\nansible_ssh_private_key_file=cloud.pem")
    my_file.close()
else:
    print "Cannot create instance"

# reservations = connection.get_all_reservations()
# for reservation in reservations:
#     for instance in reservation.instances:
#         pprint(instance.__dict__)
