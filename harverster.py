# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import threading
import time
import datetime
import tweepy
import json
import os
import api_key as mykey
from db_processor import DBProcessor
from multiprocessing.dummy import Pool as ThreadPool


def load_api(consumer_key, consumer_secret, access_token, access_token_secret):
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    return tweepy.API(auth)


def search_tweets_by_query(api, query, geocode, max_tweets=450):
    result_tweets = []
    while len(result_tweets) < max_tweets:
        count_of_remaining = max_tweets - len(result_tweets)
        try:
            new_tweets = api.search(query, count=100 if 100<count_of_remaining else count_of_remaining, geocode=geocode)
            result_tweets.extend(new_tweets)
        except tweepy.TweepError:
            print("reach rate limit")
            print('(until:', datetime.datetime.now() + datetime.timedelta(minutes=15), ')')
            time.sleep(60*15)
            break

    return result_tweets


def write_to_files(tweets):
    with open("twitter_json.json", 'a') as f:
        for tweet in tweets:
            json.dump(tweet._json, f)
            f.write('\n')


def filter_tweets_with_geo(tweets):
    for i in range(len(tweets)-1, -1, -1):
        if tweets[i]._json["coordinates"] is None and tweets[i]._json["place"] is None:
            tweets.pop(i)

    # Could be None
    return tweets


class HarvesterStreamListener(tweepy.StreamListener):

    def __init__(self, db_instance, api=None):
        super().__init__(api)
        self.db_instance = db_instance

    def on_status(self, status):
        filter_tweet_in_list = filter_tweets_with_geo([status])
        self.db_instance.save_tweets(filter_tweet_in_list)
        # print("Stream running ",threading.get_ident(), "\n")

    def on_error(self, status_code):
        if status_code == 420:
            # reach rate limit
            print("Stream ID: ", threading.get_ident(), "reach rate limited")
            return False


def harvest_by_stream_single_process(api_key, db_instance, interval):

    time.sleep(interval*30)
    try:
        stream_listener = HarvesterStreamListener(db_instance)
        api = load_api(api_key["consumer_key"], api_key["consumer_secret"],
                       api_key["access_token"], api_key["access_token_secret"])
        harvest_stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
        harvest_stream.filter(locations=[144.593741856, -38.433859306, 145.512528832, -37.5112737225])
    except AttributeError:
        # compensate for the tweepy stream bug
        print("a bug from tweepy. Stop this thread", threading.get_ident())
        pass


def harvest_single_process(api_key, db_instance, interval):

    api = load_api(api_key["consumer_key"], api_key["consumer_secret"],
                   api_key["access_token"], api_key["access_token_secret"])
    while True:
        time.sleep(interval*60)
        result_tweets = search_tweets_by_query(api, " ", "-37.8140000,144.9633200,100km")
        filtered_tweets = filter_tweets_with_geo(result_tweets)
        db_instance.save_tweets(filtered_tweets)
        # print("ID: ", threading.get_ident()," finish at ", datetime.datetime.now(), "and finds ", len(filtered_tweets)
        #       , " tweets. ")
        # print(os.linesep)
        time.sleep(60*15)


def harvest_runner(api_keys_REST, api_keys_Stream, db_instances_REST, db_instances_Stream):
    intervals_REST = [i for i in range(0, len(api_keys_REST))]
    intervals_Stream = [i for i in range(0, len(api_keys_Stream))]
    pool = ThreadPool(len(api_keys_REST)+len(api_keys_Stream))
    # pool.starmap(harvest_single_process, zip(api_keys, db_instances, intervals))
    branch_mark_REST = ["REST"] * len(api_keys_REST)
    branch_mark_Stream = ["Stream"] * len(api_keys_Stream)
    pool.starmap(threading_branch, zip(branch_mark_REST+branch_mark_Stream, api_keys_REST+api_keys_Stream,
                                       db_instances_REST+db_instances_Stream, intervals_REST+intervals_Stream))
    pool.close()
    pool.join()


def threading_branch(branch, api_key, db_instance, interval):
    if branch == "REST":
        harvest_single_process(api_key, db_instance, interval)
    elif branch == "Stream":
        harvest_by_stream_single_process(api_key, db_instance, interval)


def initialise_db_instances_list(db_name, counts):
    dbs = []
    for i in range(0,counts):
        dbs.append(DBProcessor(db_name))

    return dbs
# dbs = []
# dbs_stream = []
# for i in range(0, len(mykey.api_keys)):
#     dbs.append(DBProcessor("twitter_storage"))


# harvest_runner(harvest_single_process, mykey.api_keys_REST, initialise_db_instances_list("twitter_storage",
#                                                                                     len(mykey.api_keys_REST)))
# harvest_runner(harvest_by_stream_single_process, mykey.api_keys_stream,
#                initialise_db_instances_list("twitter_storage", len(mykey.api_keys_stream)))

harvest_runner(mykey.api_keys_REST, mykey.api_keys_stream,
               initialise_db_instances_list("twitter_storage", len(mykey.api_keys_REST)),
               initialise_db_instances_list("twitter_storage",len(mykey.api_keys_stream)))
