# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import couchdb


class DBProcessor():

    def __init__(self, db_name, address = None):
        if address:
            self.couch = couchdb.Server(address)
        else:
            self.couch = couchdb.Server()
        self.db_name = db_name
        self.db = self.couch[db_name]

    def save_tweets(self, tweets):
        for tweet in tweets:
            tweet_json = tweet._json
            self.db.save(tweet_json)
